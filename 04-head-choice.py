# /usr/bin/python
#-*- coding: utf-8-*-
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# -------------------------------------
# $ head.py -n 2 -f file.txt
# $ head.py < file.txt
# $ head.py -n 3
# $ head.py -f file.txt
# tots els altres casos d'error
# -------------------------------------
import sys,argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primeres línies """,\
        epilog="Final epilog")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies 5|10|15",dest="nlin",\
        metavar="numLines",default=10,choices=[5,10,15])
parser.add_argument("file",type=str,\
        help="fitxer a processar", metavar="file")
args=parser.parse_args()
print args
# -------------------------------------------------------
MAXLIN=args.nlin
fileIn=open(args.file,"r")
counter=0
for line in fileIn:
  counter+=1
  print line,
  if counter==MAXLIN: break
fileIn.close()
exit(0)

