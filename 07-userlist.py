#! /usr/bin/python
#-*- coding: utf-8-*-
# -------------------------------------
# @ edt Sergi Muñoz ASIX M06 Curs 2018-2019
# -------------------------------------
# Programa list users 
# -------------------------------------
import sys,argparse

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,line):
    "Constructor objectes UnixUser"
    list_camps=line.split(':')
    self.login=list_camps[0]
    self.passwd=list_camps[1]
    self.uid=int(list_camps[2])
    self.gid=int(list_camps[3])
    self.gecos=list_camps[4]
    self.homes=list_camps[5]
    self.shell=list_camps[6]
  def show(self):
      print "login: %s , passwd: %s , uid: %d , gid: %d , gecos: %s , homes: %s , shell %s" % (self.login, self.passwd, self.uid, \
        self.gid, self.gecos, self.homes, self.shell)
  def sumaun(self):
    "Funció que suma 1 al uid"
    self.uid+=1
  def __str__(self):
    "Funcio to_string"
    return " %s %d %d" % (self.login, self.uid, self.gid)

parser = argparse.ArgumentParser(description=\
        """Programa que crea instancies UnixUsers """,\
        epilog="Final epilog")
parser.add_argument("-f","--file",type=str,\
        help="user or file stdin", default="/dev/stdin",  metavar="file",dest="files")
args=parser.parse_args()
print args
#------------------------------------
userlist=[]
fileIn=open(args.files,"r")
for line in fileIn:
    user=UnixUser(line)
    userlist.append(user)
fileIn.close()
for user in userlist:
    print user
exit(0)

