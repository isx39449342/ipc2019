#!/usr/bin/python
#-*- coding: utf-8-*-
# Programa que mostra les 10 primeres línies d'un file
# Date:9/01/2019
# ----------------------------------------------------
import sys,argparse
parser = argparse.ArgumentParser(description="""exemple de mostrar les primeres linies""",\
        prog="exemple.py", epilog="thats all folks")

parser.add_argument("-n","--nlin",type=int,help="num lin (int)", metavar="les linies",default=10)
parser.add_argument("-f",type=str,help="fitxer", metavar="fitxer", default="/dev/stdin")
args=parser.parse_args()
print args
#----------------------------------------------------
MXLINE=args.nlin
count=0
fileIn=sys.stdin
if len(sys.argv) == 2:
    fileIn=open(sys.argv[1],"r")
for line in fileIn:
    count+=1
    print line,
    if count == MXLINE : break
fileIn.close()
exit(0)


