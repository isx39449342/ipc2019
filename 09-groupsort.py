#! /usr/bin/python
#-*- coding: utf-8-*-
# -------------------------------------
# @ edt Sergi Muñoz ASIX M06 Curs 2018-2019
# -------------------------------------
# Programa list users 
# -------------------------------------
import sys,argparse

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,line):
    "Constructor objectes UnixUser"
    list_camps=line.split(':')
    self.login=list_camps[0]
    self.passwd=list_camps[1]
    self.uid=int(list_camps[2])
    self.gid=int(list_camps[3])
    self.gecos=list_camps[4]
    self.homes=list_camps[5]
    self.shell=list_camps[6]
  def show(self):
      print "login: %s , passwd: %s , uid: %d , gid: %d , gecos: %s , homes: %s , shell %s" % (self.login, self.passwd, self.uid, \
        self.gid, self.gecos, self.homes, self.shell)
  def sumaun(self):
    "Funció que suma 1 al uid"
    self.uid+=1
  def __str__(self):
    "Funcio to_string"
    return " %s %d %d" % (self.login, self.uid, self.gid)

class UnixGroup():
  """Classe UnixGroup: prototipus de /etc/group
	gname:passwd:gid:listUsers"""
  
  def __init__(self,line):
    "Constructor objectes UnixGroup"
    list_camps=line.split(':')
    self.gname=list_camps[0]
    self.passwd=list_camps[1]
    self.gid=int(list_camps[2])
    list_users=list_camps[3].split(',')
    self.listusers=list_users
    if self.listusers == "[\n]":
		self.listusers =""
  def show(self):
      print "gname: %s , passwd: %s , gid: %d , listusers %s" % (self.gname, self.passwd, self.gid, \
        self.listusers)
  def sumaun(self):
    "Funció que suma 1 al uid"
    self.uid+=1
  def __str__(self):
    "Funcio to_string"
    return " %s %d %s" % (self.gname, self.gid, self.listusers)

		
parser = argparse.ArgumentParser(description=\
        """Programa que crea llista de users amb gname """,\
        epilog="Final epilog")

parser.add_argument("-u","--users", help=" prog.py [-s login|gid|gname]  -u fileusers -g fileGroup", \
dest="fileusers" ,default="/dev/stdin")

parser.add_argument("-g","--group", help="[-s login|gid|gname]  -u fileusers -g fileGroup", \
dest="filegroup")

#parser.add_argument("-s","--sort",\
#        help="ordenar per login o gid o gname" , choices=[ "login","gid","gname" ])

args=parser.parse_args()
print args

#--------------------------------
groupdicc={}
userlist=[]

fileInGrp=open(args.filegroup,"r")
for line in fileInGrp:
	ugroup=UnixGroup(line)
	groupdicc[ugroup.gid]=ugroup
fileInGrp.close()

for group in groupdicc:
	print groupdicc[group]
#sys.exit(0)

fileInUsr=open(args.fileusers,"r")
for line in fileInUsr:
    user=UnixUser(line)
    userlist.append(user)
fileInUsr.close()

for user in userlist:
	print user


#rint groupdicc
exit(0)
