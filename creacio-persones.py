# /usr/bin/python
#-*- coding: utf-8-*-
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# -------------------------------------
# $ Exemple de creació de classes i objectes
# -------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,l,i,g):
    "Constructor objectes UnixUser"
    self.login=l
    self.uid=i
    self.gid=g
  def show(self):
    print "login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid)
  def sumaun(self):
    "Funció que suma 1 al uid"
    self.uid+=1
  def __str__(self):
    "Funcio to_string"
    return " %s %d %d" % (self.login, self.uid, self.gid)

user1=UnixUser("sergi",15,100)
user1.show()
user1.sumaun()
user1.show()
l=[13,"juny",user1,user2]
l.show(user1)
