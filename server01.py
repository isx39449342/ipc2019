# /usr/bin/python
#-*- coding: utf-8-*-
# -------------------------------------
# @ edt Sergi Muñoz Carmona ASIX M06 Curs 2018-2019
# date: 13/02/2019
# Server examen M06
# -------------------------------------
import sys,socket,argparse,os,signal
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Servidor que processa comandes""")
parser.add_argument("-d","--debug",type=int, default=1)
parser.add_argument("-p","--port",type=int, default=44444)
args=parser.parse_args()
FI=chr(4)
HOST = ''
PORT = args.port
DEBUG = args.debug
llistaPeers=[]

def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  print len(llistaPeers)
  sys.exit(0)

def mysigterm(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers, len(llistaPeers)
  sys.exit(0)

signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

pid=os.fork()
if pid!=0:
	print "Engegat el server:",pid
	sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print "Connected by", addr
  llistaPeers.append(addr)
  while True:
    data = conn.recv(1024)
    command=[data]
    if not command: break
    pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
    for line in pipeData.stdout:
      if DEBUG: sys.stderr.write(line)
      conn.sendall(line)
    for line in pipeData.stderr:
      if DEBUG: sys.stderr.write(line)
      conn.sendall(line)
    conn.sendall(FI)
  conn.close()
s.close()
sys.exit(0)

##if command == "processos":
	##command = "ps ax"
##if command == "ports":
	##command = " netstat -p"
##elif command == "whoareyou":
	##command = "uname -a"
##else:
	##command = "uname -a"

