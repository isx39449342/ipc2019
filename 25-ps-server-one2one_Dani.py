# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,argparse, signal,os,datetime
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """CAL server""",\
        epilog="thats all folks")
parser.add_argument("-a","--anyo",type=int,\
        help="anyo a mostrar",dest="anyo",\
        metavar="anyo",default=2019)
parser.add_argument("-p","--puerto",type=int,\
        help="numero de puerto", metavar="puerto",\
        default=50001,dest="puerto")
args=parser.parse_args()
# -------------------------------------------------------
def mysigusr1(signum,frame):
	global upp
	print "Signal handler called with signal:", signum
	print llistaPeers	
	sys.exit(0)

def mysigusr2(signum,frame):
	print "Signal handler called with signal:", signum
	print len(llistaPeers)
	sys.exit(0)

def mysigterm(signum,frame):
	print "Signal handler called with signal:", signum
	print llistaPeers,len(llistaPeers)
	sys.exit(0)
# -------------------------------------------------------

    
pid=os.fork()
if pid!=0:
	print "Engegat el server:",pid
	sys.exit(0)
	  
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

llistaPeers=[]
HOST = ''
PORT = args.puerto
fileout=""
#generem socker
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#per reusar la coneccio i utilitzar el mateix port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#En qui port i adreça estarà lligat
s.bind((HOST,PORT))
#escoltem
s.listen(1)
while True:
	#conneccio i adreça de cada client
	conn,addr = s.accept()
	llistaPeers.append(addr)
	fileout=open("/tmp/%s-%d-%s.log" % (addr[0],PORT,datetime.datetime.now().strftime("%Y%m%d-%I%M%S")),"w")
	while True:
		data = conn.recv(1024)
		if not data: break
		fileout.write(data)
	fileout.close()
	conn.close()
sys.exit(0)


